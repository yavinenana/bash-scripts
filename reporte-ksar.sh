#!/bin/bash
# Creates a PDF with tool Ksar [java]
# usage:
# reporte-ksar.sh [arin] [sa15] ["07:00:00"] ["21:00:00"]

#set -x

init="07:00:00"
ending="21:00:00"

NAME="${1:-variable1}"
SA="${2:-variable3}"
INTERVAL_INIT="${3:-$init}"
INTERVAL_ENDING="${4:-$ending}"
DATE=$(date +%d%m%Y-%H:%M)
PDFNAME=$NAME-$SA-$DATE

/usr/local/bin/aws s3 cp s3://backupsyaros/test/$SA . 
LC_ALL="es_PE.UTF-8" /usr/bin/sar -A -s $INTERVAL_INIT -e $INTERVAL_ENDING -f $SA >> $SA.txt
/usr/bin/java -jar /home/$USER/Downloads/ksar/kSar-5.0.6/kSar.jar -input $SA.txt -outputPDF $PDFNAME.pdf

# Deleted download SA
rm -rf $SA*

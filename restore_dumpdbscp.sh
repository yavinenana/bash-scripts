#!/bin/bash
# create folder in home ubuntu named bk
# create .pgpass with password and roles database
# ./create_db.sh odoo database bucket folder/subfolder/ profile

#set -x
#PATH="/usr/local/bin:/usr/bin:/bin"

odoo="${1:-$odoo}"
database="${2:-$database}"
bucket=backups-yaros
bucket="${3:-$bucket}"
path_bucket="${4:-$path}"
profile=default
profile=${5:-$profile}
zip_db="$(/usr/local/bin/aws s3 ls s3://$bucket/$path_bucket --profile $profile | sort | tail -n 1 | awk '{print $4}')"
#zip_db=`aws s3 ls s3://$bucket/dedalo/dedalo/ | sort | tail -n 1 | awk '{print $4}'`

echo "1.  downloading: $database ...\n aws s3 cp s3://$bucket/$path_bucket$zip_db --profile $profile /opt/bk/"

sudo mkdir -p /opt/bk/
sudo chown -R ubuntu:ubuntu /opt/bk/

/usr/local/bin/aws s3 cp s3://$bucket/$path_bucket$zip_db --profile $profile /opt/bk/
echo "2.  print bucket bath : zip_db : database"
echo $path_bucket
echo $zip_db
echo $database
echo $profile
echo "3.  conditionals to determinate ZIP format"
val="$(echo $zip_db | cut -d. -f2)"
echo $val
if [ $val == 'zip' ]
then
        echo "-------------zip format"
        echo "$val=='zip'"
        cd /opt/bk/ && unzip $zip_db
else
        echo "-------------gzip"
        echo "$val != 'zip' ... so is a gzip"
        cd /opt/bk/ && gzip -d *
fi

#gzip -d /opt/bk/* 
echo "stop $odoo"
#sudo /etc/init.d/$odoo stop
sudo supervisorctl stop $odoo
echo "drop $database"
sudo -u postgres psql -c "drop database $database;"
echo "create $database"
sudo -u postgres psql -c "create database $database with owner $odoo;"
ls /opt/bk/
set -x
sql_db="$(ls /opt/bk/ | grep *.sql)"
psql -U $odoo -h 0.0.0.0 -p 5432 -d $database < /opt/bk/$sql_db
rm -rf /opt/bk/*
#sudo /etc/init.d/$odoo restart
sudo supervisorctl restart $odoo

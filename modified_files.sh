#!/bin/bash
set var1 [lindex $arg 0]
set var2 [lindex $arg 1]
entry=$(ls /usr/local/nagios/etc/objects/nagios_cfg/srv12)
echo ${entry[@]}
set -x 
for i in ${entry[@]}
do
        echo $i
        sudo sed -i '/check_http_url/s/$/ -S/' /usr/local/nagios/etc/objects/nagios_cfg/srv12/$i
done

#ls /usr/local/nagios/etc/objects/nagios_cfg/srv12 | while read line
#do 
#       echo -e '$ line \n'
#done

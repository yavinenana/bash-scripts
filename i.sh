#!/bin/bash

version="$(sudo -u postgres psql -c "show server_version" | grep 9)"
if ($version=='9.1.*'):
	$version=9.1
elif ($version=='9.2.*'):
	$version=9.2
elif ($version=='9.3.*'):
	$version=9.3
elif ($version=='9.4.*'):
	$version=9.4
elif ($version=='9.5.*')
	$version=9.5
elif ($version=='9.6.*')
        $version=9.6
elif ($version=='10.*')
        $version=10

echo $VERSION
echo $VERSION
echo $VERSION
cd /etc/postgres/$version/

#!/usr/bin/expect -f
# usage of script 
# ./expect_copy_build.sh /home/rodrigo/carpetad3 /tmp/ <dest_ip> root <root_password>

set buildsourcefolder [lindex $argv 3]
set destinationpath [lindex $argv 4]
set ip [lindex $argv 0]
set user [lindex $argv 1]
set password [lindex $argv 2]

if {$buildsourcefolder ==""} {
puts "No build source folder specified ... so exiting the script";
exit
}
if {$destinationpath ==""} {
puts "No destination path specified ... so exiting the script";
exit
}
if {$ip ==""} {
puts "No destination ip specified ... so exiting the script";
exit
}
if {$user ==""} {
puts "No user specified ... so exiting the script";
exit
}
if {$password ==""} {
puts "No password ... so exiting the script";
exit
}

spawn scp -r $buildsourcefolder $user@$ip:$destinationpath
expect "password:"
send "$password\r"
interact

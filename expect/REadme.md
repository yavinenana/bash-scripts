expect "Hola\n"; send "has escrito $expect_out(buffer)"; send "pero solo debias poner $expect_out(0,string)"

...
#!/usr/bin/expect
expect "Hola\n"
send "has escrito <$expect_out(buffer)>"
send "\n pero solo debias poner <$expect_out(0,string)>"

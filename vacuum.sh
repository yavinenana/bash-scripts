#!/bin/bash
# HOW TO USE
# ./vacuum [database] [role]

#set -x
export PGPASSWORD='odoo'
DATE=$(date +%d%m%Y)

database="${1:-$database}"
#database="$(sudo -u postgres /usr/bin/psql -c "\l+" | grep odoo | tail -n 1 | awk '{print $1}' | head -1)"
role="${2:-$role}"
#role='odoo'

sudo -u postgres psql -c --host=127.0.0.1 --port=5432 --dbname=$database -c "\l+ $database;" >> /home/$USER/vacuum-$database-$DATE.txt 2>&1
sudo -u postgres psql -c --host=127.0.0.1 --port=5432 --dbname=$database -c "vacuum full analyze ;" >> /home/$USER/vacuum-$database-$DATE.txt 2>&1
sudo -u postgres psql -c --host=127.0.0.1 --port=5432 --dbname=$database -c "\l+ $database;" >> /home/$USER/vacuum-$database-$DATE.txt 2>&1
#psql --host=127.0.0.1 --port=5432 --dbname=creae -e PGPASSWORD="$(pbpaste)" --username=odoo -c "vacuum full full analyze verbose;" >> /home/ubuntu/select.txt 2>&1

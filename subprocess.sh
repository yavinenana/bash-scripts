#!/usr/bin/expect -f
# usage of script 
# ./expect_copy_build.sh proyecto /tmp/ <dest_ip> root <root_password>

set proyecto [lindex $argv 0]
set proyecto [lindex $argv 1]
set password [lindex $argv 2]
set buildsourcefolder [lindex $argv 3]
set destinationpath [lindex $argv 4]
set destinationpath2 [lindex $argv 5]

if {$buildsourcefolder ==""} {
puts "No build source folder specified ... so exiting the script";
exit
}
spawn scp -r $buildsourcefolder $user@$ip:$destinationpath
expect "password:"
send "$password\r"
interact
spawn scp -r $buildsourcefolder $user@$ip:$destinationpath2
expect "password:"
send "$password\r"
interact


import hudson.model.*
import hudson.jenkins.*
import hudson.jenkins.model.* 
import hudson.jenkins.hudson.* 


for(project in hudson.model.Hudson.instance.items) { 
  for(build in project.builds) {    
    manager.createSummary("warning.gif").appendText("<h1>"+build.buildVariables.get("GUMSHIELD")+"</h1>", false, false, false, "red")  
  }  
} 
def jobName = "jobName"
def job = Jenkins.instance.getItem(jobName)
job.getBuilds().each { it.delete() }
job.nextBuildNumber = 1
job.save()

def jobs = Jenkins.instance.projects.collect { it } 
jobs.each { job -> job.getBuilds().each { it.delete() }} 

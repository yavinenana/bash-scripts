#!/bin/bash
#PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games
sudo /etc/init.d/nginx stop
cd /opt/letsencrypt/
sudo ./letsencrypt-auto --renew-by-default certonly --standalone --email jordy@yaroslab.com -d heidelberg.yaroscloud.com
#sudo ./letsencrypt-auto --renew-by-default certonly --standalone --email jordy@yaroslab.com -d test.yaroscloud.com
sudo /etc/init.d/nginx start

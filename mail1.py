import smtplib

sender = "Desde local <jordy@yaroslab.com>"
receiver = "EL destinatario <yavinenana@gmail.com>"

asunto = "Email HTML enviado desde Python"
message = """HOLA!<br/><br>
Este es un <b>e-mail</b> enviado desde <b>Python</b>
 """

email = """From: %s
To: %s
MIME-Version: 1.0
Content-Type: text/html
Subject: %s

%s
""" % (sender,receiver,asunto,message)
try:
    smtp=smtplib.SMTP('localhost')
    smtp.sendmail(sender,receiver,email)
    print "correo enviado"
except:
    print """Error: el mensaje no pudo enviarse.
    Compruebe que sendmail se encuentra instalado en su sistema"""


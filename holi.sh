#!/bin/bash
# you can use docker docker exec -it ubuntu16 /bin/bash -c "apt install curl"
# to exec command in docker 
cd ~/havas-compose/src/openerp/version80 && gi pull
cd ~/havas-compose/
docker-compose -f havas-compose.yml stop && docker-compose -f havas-compose.yml up
exit


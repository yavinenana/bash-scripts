#!/bin/bash
cd dockbeat
git clone https://github.com/Ingensi/dockbeat.git
docker build -t dockbeat:${GIT_COMMIT} .
docker tag dockbeat:${GIT_COMMIT} 10.34.80.116:5000/dockbeat:${GIT_COMMIT}
docker push 10.34.80.116:5000/dockbeat:

#!/bin/bash
# create folder in home ubuntu named bk
# create .pgpass with password and roles database
# ./create_db.sh odoo database folder/subfolder/
odoo="${1:-$odoo}"
database="${2:-$database}"
path_bucket="${3:-$path}"
zip_db="$(aws s3 ls s3://backupsyaros/$path_bucket/ | sort | tail -n 1 | awk '{print $4}')"
#zip_db=`aws s3 ls s3://backupsyaros/dedalo/dedalo/ | sort | tail -n 1 | awk '{print $4}'`

echo "1.  downloading: $database ...\n aws s3 cp s3://backupsyaros/$path_bucket$zip_db /opt/bk/"
touch .pgpass
echo "0.0.0.0:5432:*:odoo:odoo" >> .pgpass
chmod 0600 ~/.pgpass
#mkdir -p /opt/bk/
aws s3 cp s3://backupsyaros/$path_bucket/$zip_db /opt/bk/
echo "2.  print bucket bath : zip_db : database"
echo $path_bucket
echo $zip_db
echo $database
echo "3.  conditionals to determinate ZIP format"
val="$(echo $zip_db | cut -d. -f2)"
echo $val
if [ $val == 'zip' ]
then
	echo "-------------zip format"
	echo "$val=='zip'"
	cd /opt/bk/ && unzip $zip_db
else
	echo "-------------gzip"
	echo "$val != 'zip' ... so is a gzip"
	cd /opt/bk/ && gzip -d *
fi

#descompress /opt/bk/* 
echo "stop $odoo"
sudo /etc/init.d/$odoo stop
echo "drop $database"
sudo -u postgres psql -c "drop database $database;"
echo "create $database"
sudo -u postgres psql -c "create database $database with owner $odoo;"
ls /opt/bk/
set -x
sql_db="$(ls /opt/bk/ | grep *.sql)"
psql -U $odoo -h 0.0.0.0 -p 5432 -d $database < /opt/bk/$sql_db
sudo rm -rf /var/lib/odoo/.local/share/Odoo/filestore/$database
sudo mv /opt/bk/filestore /var/lib/odoo/.local/share/Odoo/filestore/$database
sudo chown -R odoo:odoo /var/lib/odoo/.local/share/Odoo/filestore/$database
rm -rf /opt/bk/*
sudo /etc/init.d/$odoo restart

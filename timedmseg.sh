#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Argument Error.  Please specify the Time interval in seconds."
    exit
fi

timeinterval=$1 # change this to the desired time interval
currenttime=$(date +%s)

string="$(who -b | awk '{print $3"-"$4}')"

IFS='-' read -a array <<< "$string"

MONTHS=(X Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec)

year="${array[0]}"
month=${MONTHS[${array[1]}]}
day="${array[2]}"
hour="${array[3]}"

daystring="$month $day $year $hour"

bootime=$(date --date="$daystring" +%s)

dmesg | while read line; do 
    dmesgstamp=$(echo "$line" | awk -F'[' '{print $2}' | awk -F\. '{print $1}')
    dmesgstamp=$((dmesgstamp += bootime))
    results=$((currenttime-dmesgstamp))
    if ((results < timeinterval)); then
        echo "$line"
    fi
done

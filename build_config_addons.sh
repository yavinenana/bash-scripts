# /usr/bin/expect -f

# copy config into /postgresql.conf ---configs to profiler and some performance
#v_psql="$(sudo -u postgres psql -c "show server_version" | grep 9)"
#v_psql=9.5
# bk postgresqlconfig

#set proyecto [lindex $argv 1]
set proyecto [lindex $argv 0]

if {}

cd /etc/postgresql/
v_psql="$(ls)"
cp /etc/postgresql/$v_psql/main/postgresql.conf /tmp/
vim /etc/postgresql/9.5/main/postgresql.conf +443d -c wq
echo "logging_collector=on" | sudo tee -a /etc/postgresql/$v_psql/main/postgresql.conf
echo "log_destination='stderr'" | sudo tee -a /etc/postgresql/$v_psql/main/postgresql.conf
echo "log_directory='$HOME/postgres'" | sudo tee -a /etc/postgresql/$v_psql/main/postgresql.conf
echo "log_filename='postgresql.log'" | sudo tee -a /etc/postgresql/$v_psql/main/postgresql.conf
echo "log_rotation_age=0" | sudo tee -a /etc/postgresql/$v_psql/main/postgresql.conf
echo "log_checkpoints=on" | sudo tee -a /etc/postgresql/$v_psql/main/postgresql.conf
echo "log_hostname=on" | sudo tee -a /etc/postgresql/$v_psql/main/postgresql.conf
echo "log_line_prefix='%t [%p]: [%l-1] db=%d,user=%u '" | sudo tee -a /etc/postgresql/$v_psql/main/postgresql.conf
vim -c ":443s/$/log_timezone = 'UTC' /|wq" /etc/postgresql/$v_psql/main/postgresql.conf
set -x
usuario=$USER

#echo "text" | sudo tee -a /path/file
sudo apt-get install pgtop runsnakerun pgbadger -y && python /usr/lib/python2.7/dist-packages/runsnakerun/runsnake.py &
mkdir $HOME/postgres
touch $HOME/postgres/postgresql.log
sudo chown -R postgres:postgres $HOME/postgres
sudo chmod 640 $HOME/postgres/postgresql.log
sudo usermod -aG postgres $usuario
sudo /etc/init.d/postgresql restart
tail -f $HOME/postgres/postgresql.log &


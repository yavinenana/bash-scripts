#!/bin/bash
echo "Introduzca un numero: "
read input
if [ $input -lt 5 ]; then
    echo "El numero era menor que 5"
elif [ $input -eq 5 ]; then
    echo "El numero era 5"
elif [ $input -gt 5 ]; then
    echo "El numero era mayor que 5"
else
    echo "No introdujo un numero"
fi

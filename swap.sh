#!/bin/bash
# this file create swap space in folder /swapfile with size value 
# to save log >> /home/ubuntu/pgbackup.log 2>&1
# run with sudo
# swap.sh [size(GB)]
size="${1:-$size}""G"
#size="$size"
#echo $size
#mkdir -p /swapfile
fallocate -l "$size" /swapfile
chmod 600 /swapfile
#ls -lh /swapfile
mkswap /swapfile
swapon /swapfile
#sudo swapon --show
cp /etc/fstab /etc/fstab.bak
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
# sudo bash -c 'echo '/swapfile none swap sw 0 0' >> /etc/fstab'
## remove swapfile
## swapoff /swapfile
## rm /swapfile

aws ec2 run-instances --image-id ami-cd0f5cb6 --count 1 \
	--instance-type t2.small --key-name 2015_cloud \
	--subnet-id subnet-10529f67 \
	--block-device-mappings "[{\"DeviceName\":\"/dev/sda1\",\"Ebs\":{\"VolumeSize\":10,\"DeleteOnTermination\":true}}]" \
	--security-group-ids sg-3515d944 \
